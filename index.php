<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Documento sin título</title>
        <link href="dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <br />
        <div class="row"><?php echo 'Versión actual de PHP: ' . phpversion();?>
            <div class="col-lg-4 col-offset-4"><fieldset>
                <legend>Crear funcion con estilo Camel case</legend>
                <form action="" method="post" class="bs-example"><label>Ejemplo: lista usuarios por id  &nbsp;
                    <input name="NOMBRE_FUNCION" type="text"  class="input-large" id="NOMBRE_FUNCION" size="50"/></label>
						
					<label>Codigo<br />
                    <textarea name="CODIGO_FUNCION" cols="50" rows="5" class="input-large" id="CODIGO_FUNCION"></textarea>
					</label>
                    <button name="ENVIAR" type="submit" value="CREAR" id="ENVIAR"  class="btn btn-primary"/><i class="icon-ok"></i>
                    CREAR NOMBRE DE FUNCION CAMEL CASE
                    </button>
                </form>
                </fieldset>
        </div>  
        </div>
    <?php 
	header('X-XSS-Protection:0');
	/*echo "<fieldset> <legend>NOMBRE_FUNCION</legend><pre>"; 
print_r($_POST);
echo "</pre></fieldset>";
die();*/	



if(isset($_POST["ENVIAR"])){
    if($_POST["ENVIAR"]!=''){

        $fun=explode(" ",strtolower($_POST["NOMBRE_FUNCION"]));
        $nombrefinal="";
        $nombreguion="";
        $nombreguionmedio="";
		$nombresinguion="";	
        foreach($fun as $funcion){
            $nombrefinal.=ucfirst($funcion);
            $nombreguion.=ucfirst($funcion)."_";	
            $nombreguionmedio.=($funcion)."-";	
			$nombresinguion.=ucfirst($funcion);
        }
        $nombreguion=substr($nombreguion, 0, -1);                            
        $nombreguionmedio=substr($nombreguionmedio, 0, -1);                            
    ?>
    <div class="row">
        <div class="col-lg-4 col-offset-4">
            <fieldset><legend>Camel case <img src="Laravel_logo.png" width="211" height="74" /></legend>
                <div class="row">
                    <div class="col-lg-4 col-offset-4">
                        <code>
                            public static function <?php echo "set".$nombrefinal;?>(){<br>

                                try{<br>
                                $query=("tu sql ");<br>	
                                <!--
                                /*	echo "&ltfieldset&gt&nbsp;&ltlegend&gt&lt/legend&gt&ltpre&gt"; <br />
                                print_r($query);<br />
                                echo "&lt/pre&gt&lt/fieldset&gt";

                                */-->

                                <br>
                                $results = DB::select($query, array($parametro1,$parametro1));<br>
                                return $results;<br>

                                } catch (Exception $e) {<br>
                                report_error($e);<br>
                                return array();<br>
                                }<br>

                                }

                        </code>

                        <br><br>
                    </div>
                </div></fieldset>
        </div>
        <br /><br /><br /> <br /><br /><br />
        <div class="col-lg-6 col-offset-4">
            <fieldset>
                <legend>Camel case <img src="PHP_codeigniter.png" width="300" height="80" /></legend>
                <div class="row">
				<legend>queri corta CI4</legend>
				<div class="col-lg-12">
                        <code>
                            function <?php echo $nombrefinal;?>(){<br>
                               

                            $ql=("tu sql ");<br>	
                            //print_r($ql);<br>
                            $this->obj_result = $this->db->query($ql); <br>
						    if ($this->obj_result->getNumRows() > 0){ <br>
                            $fila = $this->obj_result->getResultArray();<br>
                            return $fila;<br>
                            }<br>

                            }
                        </code>
                        <br> <br> <br>
                       
                        <br>
                    </div>
                <hr>
				<legend>queri corta CI3</legend>
				<div class="col-lg-12">
                        <code>
                            function <?php echo $nombrefinal;?>(){<br>


                            $ql=("tu sql ");<br>	
                            //print_r($ql);<br>
                            $this->obj_result = $this->db->query($ql); <br>
						    if ($this->obj_result->num_rows() > 0){ <br>
                            $fila = $this->obj_result->result_array();<br>
                            return $fila;<br>
                            }<br>

                            }
                        </code>
                        <br> <br> <br>
                       
                        <br>
                    </div>
				
				
				
				
				
				 <br /><br /><br /> <br /><br /><hr /><br /> <br /><br /><br />
				 
				
					<legend>queri larga</legend>
				
                    <div class="col-lg-12">
                        <code>
                            function <?php echo "set".$nombrefinal;?>(){<br>


                            $ql=("tu sql ");<br>	
                            //print_r($ql);<br>
                            $this->obj_result = $this->db->query($ql);


                            }
                        </code>
                        <br> <br> <br>
                        <code>
                            function <?php echo "get".$nombrefinal;?>(){<br>

                            if ($this->obj_result->num_rows() > 0){ <br>
                            $fila = $this->obj_result->result_array();<br>
                            return $fila;<br>
                            }<br>

                            }
                        </code>
                        <br>
                    </div>
                </div></fieldset>
        </div>

        <br /><br /><br /> <br /><br /><br /> <br /><br /><br />

        <div class="col-lg-4 col-offset-4">
            <fieldset><legend>Camel case con guiones bajos</legend>
                <div class="row">
                    <div class="col-lg-4 col-offset-4">
                    <code>
                            function <?php echo $nombreguionmedio;?>(){}
                        </code><br /><br />

                        <code>
                            function <?php echo $nombreguion;?>(){}
                        </code><br /><br />
						     <code>
                            function <?php echo strtolower($nombreguion);?>(){}
                        </code><br /><br />
						
						 <code>
                            function post<?php echo ucfirst(strtolower($nombresinguion));?>(){}
                        </code><br /><br />
						
						 <code>
                            function get<?php echo ucfirst(strtolower($nombresinguion));?>(){}
                        </code>
                    </div>
                </div>
            </fieldset>
        </div>
		
		
		<!--
		
		 <br /><br /><br /> <br /><br /><br /> <br /><br /><br />

        <div class="col-lg-4 col-offset-4">
            <fieldset><legend>React</legend>
                <div class="row">
                    <div class="col-lg-4 col-offset-4">
                        <code>             
						 		
							
import * as React from 'react';<br />
import { StyleSheet, Text, View,TouchableOpacity,Alert } from 'react-native';<br />
import {Input } from 'react-native-elements';<br />
import {TextInput } from 'react-native-paper';<br />

export default class <?php echo $nombreguion;?> extends React.Component {<br />

  render(){<br />
    return (<br />
	 <textarea name="CODIGO_FUNCION" cols="50" rows="10" class="input-large" id="CODIGO_FUNCION" readonly="readonly"><?php  echo(($_POST["CODIGO_FUNCION"])); ?> </textarea>
    
    )<br />
  }<br />
}
	<br /><br />	
const styles=StyleSheet.create({<br />	
    restaurantEmptyView:{<br />	
        justifyContent:'center',<br />	
        flex: 1,<br />	
        marginTop: 10,<br />	
        marginBottom: 10<br />	
    },<br />	
    restaurantEmptyText:{<br />	
        backgroundColor:"#13B955",<br />	
        color:'#444',<br />	
        textAlign:'center',<br />	
        padding: 20,<br />	
    }<br />	
});			<br />			
							
                        </code><br />
						
						
						<br />
						    
                    </div>
                </div>
            </fieldset>
        </div>
-->
        <?php   }
}?>
    </div>
	<br /><br /><br /> <br /><br /><br /> <br /><br /><br />
    </body>
</html>
